/*******************************************************************/
/* Author: M.Nafady                                                */
/* Date  : 29 Jan 2019                                             */
/* File   : configuration.h                                        */
/* Version : v01                                                   */
/*******************************************************************/

#ifndef DIO_PRIVATE_CONFIGURATION_H_
#ifdef DIO_PRIVATE_H_
#define DIO_PRIVATE_CONFIGURATION_H_


/*comment :Initial direction for DIO Pins */
/* Range  :                                */

/****************  Group A    -> direction   ********************************/
#define DIO_U8_PIN0_INIT_DIRECTION     DIO_U8_PIN_INIT_OUTPUT            /**********PINA0******/
#define DIO_U8_PIN1_INIT_DIRECTION     DIO_U8_PIN_INIT_OUTPUT           /**********PINA1******/
#define DIO_U8_PIN2_INIT_DIRECTION     DIO_U8_PIN_INIT_OUTPUT           /**********PINA2******/
#define DIO_U8_PIN3_INIT_DIRECTION     DIO_U8_PIN_INIT_OUTPUT           /**********PINA3******/
#define DIO_U8_PIN4_INIT_DIRECTION     DIO_U8_PIN_INIT_OUTPUT           /**********PINA4******/
#define DIO_U8_PIN5_INIT_DIRECTION     DIO_U8_PIN_INIT_OUTPUT           /**********PINA5******/
#define DIO_U8_PIN6_INIT_DIRECTION     DIO_U8_PIN_INIT_OUTPUT           /**********PINA6******/
#define DIO_U8_PIN7_INIT_DIRECTION     DIO_U8_PIN_INIT_OUTPUT           /**********PINA7******/

/**************** Set GroupB  -> direction   ********************************/

#define DIO_U8_PIN8_INIT_DIRECTION    DIO_U8_PIN_INIT_OUTPUT       /**********PINB0***********************/
#define DIO_U8_PIN9_INIT_DIRECTION    DIO_U8_PIN_INIT_OUTPUT       /**********PINB1***********************/
#define DIO_U8_PIN10_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINB2***********************/
#define DIO_U8_PIN11_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINB3***********************/
#define DIO_U8_PIN12_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINB4***********************/
#define DIO_U8_PIN13_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINB5***********************/
#define DIO_U8_PIN14_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINB6***********************/
#define DIO_U8_PIN15_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINB7***********************/

/****************  GroupC        -> direction ********************************/
#define DIO_U8_PIN16_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINC0***********************/
#define DIO_U8_PIN17_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINC1***********************/
#define DIO_U8_PIN18_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINC2***********************/
#define DIO_U8_PIN19_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINC3***********************/
#define DIO_U8_PIN20_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINC4***********************/
#define DIO_U8_PIN21_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINC5***********************/
#define DIO_U8_PIN22_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINC6***********************/
#define DIO_U8_PIN23_INIT_DIRECTION   DIO_U8_PIN_INIT_OUTPUT       /**********PINC7***********************/


/****************  Group D       -> direction  ********************************/
#define DIO_U8_PIN24_INIT_DIRECTION   DIO_U8_PIN_INIT_INPUT       /**********PIND0***********************/
#define DIO_U8_PIN25_INIT_DIRECTION   DIO_U8_PIN_INIT_INPUT       /**********PIND1***********************/
#define DIO_U8_PIN26_INIT_DIRECTION   DIO_U8_PIN_INIT_INPUT       /**********PIND2***********************/
#define DIO_U8_PIN27_INIT_DIRECTION   DIO_U8_PIN_INIT_INPUT       /**********PIND3***********************/
#define DIO_U8_PIN28_INIT_DIRECTION   DIO_U8_PIN_INIT_INPUT       /**********PIND4***********************/
#define DIO_U8_PIN29_INIT_DIRECTION   DIO_U8_PIN_INIT_INPUT       /**********PIND5***********************/
#define DIO_U8_PIN30_INIT_DIRECTION   DIO_U8_PIN_INIT_INPUT       /**********PIND6***********************/
#define DIO_U8_PIN31_INIT_DIRECTION   DIO_U8_PIN_INIT_INPUT       /**********PIND7***********************/


/*******************Set port Direction*********************************/
#define DIO_PORT_INIT_DIRECTION   DIO_PORTOUTPUT



/****************  Group A       -> Value  ********************************/
#define DIO_U8_PIN0_INIT_VALUE       DIO_U8_PIN_INIT_LOW                 /**********PINA0******/
#define DIO_U8_PIN1_INIT_VALUE       DIO_U8_PIN_INIT_HIGH                /**********PINA1******/
#define DIO_U8_PIN2_INIT_VALUE       DIO_U8_PIN_INIT_HIGH                /**********PINA2******/
#define DIO_U8_PIN3_INIT_VALUE       DIO_U8_PIN_INIT_HIGH                /**********PINA3******/
#define DIO_U8_PIN4_INIT_VALUE       DIO_U8_PIN_INIT_HIGH                /**********PINA4******/
#define DIO_U8_PIN5_INIT_VALUE       DIO_U8_PIN_INIT_HIGH                /**********PINA5******/
#define DIO_U8_PIN6_INIT_VALUE       DIO_U8_PIN_INIT_HIGH                /**********PINA6******/
#define DIO_U8_PIN7_INIT_VALUE       DIO_U8_PIN_INIT_HIGH                /**********PINA7******/


/****************  Group B       -> Value  ***********************/
#define DIO_U8_PIN8_INIT_VALUE       DIO_U8_PIN_INIT_HIGH           /**********PINB0***********************/
#define DIO_U8_PIN9_INIT_VALUE       DIO_U8_PIN_INIT_HIGH           /**********PINB1***********************/
#define DIO_U8_PIN10_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINB2***********************/
#define DIO_U8_PIN11_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINB3***********************/
#define DIO_U8_PIN12_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINB4***********************/
#define DIO_U8_PIN13_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINB5***********************/
#define DIO_U8_PIN14_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINB6***********************/
#define DIO_U8_PIN15_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINB7***********************/

/****************  Group C       -> Value  ************************************/
#define DIO_U8_PIN16_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINC0***********************/
#define DIO_U8_PIN17_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINC1***********************/
#define DIO_U8_PIN18_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINC2***********************/
#define DIO_U8_PIN19_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINC3***********************/
#define DIO_U8_PIN20_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINC4***********************/
#define DIO_U8_PIN21_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINC5***********************/
#define DIO_U8_PIN22_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINC6***********************/
#define DIO_U8_PIN23_INIT_VALUE      DIO_U8_PIN_INIT_HIGH           /**********PINC7***********************/

/****************  Group D      -> Value  ***********************/
#define DIO_U8_PIN24_INIT_VALUE      DIO_U8_PIN_INIT_HIGH          /**********PIND0***********************/
#define DIO_U8_PIN25_INIT_VALUE      DIO_U8_PIN_INIT_HIGH          /**********PIND1***********************/
#define DIO_U8_PIN26_INIT_VALUE      DIO_U8_PIN_INIT_HIGH          /**********PIND2***********************/
#define DIO_U8_PIN27_INIT_VALUE      DIO_U8_PIN_INIT_HIGH          /**********PIND3***********************/
#define DIO_U8_PIN28_INIT_VALUE      DIO_U8_PIN_INIT_HIGH          /**********PIND4***********************/
#define DIO_U8_PIN29_INIT_VALUE      DIO_U8_PIN_INIT_HIGH          /**********PIND5***********************/
#define DIO_U8_PIN30_INIT_VALUE      DIO_U8_PIN_INIT_HIGH          /**********PIND6***********************/
#define DIO_U8_PIN31_INIT_VALUE      DIO_U8_PIN_INIT_HIGH          /**********PIND7***********************/





  /****************  Port       -> Value  ********************************/
#define DIO_PORT_INIT_VALUE   DIO_PORTHIGH



/*****************************************************************************/




/****************  Group A       -> type  ********************************/
#define DIO_U8_PIN0_INIT_TYPE       DIO_U8_INIT_PULLUP                /**********PINA0******/
#define DIO_U8_PIN1_INIT_TYPE       DIO_U8_INIT_PULLUP                /**********PINA1******/
#define DIO_U8_PIN2_INIT_TYPE       DIO_U8_INIT_PULLUP                /**********PINA2******/
#define DIO_U8_PIN3_INIT_TYPE       DIO_U8_INIT_PULLUP                /**********PINA3******/
#define DIO_U8_PIN4_INIT_TYPE       DIO_U8_INIT_PULLUP                /**********PINA4******/
#define DIO_U8_PIN5_INIT_TYPE       DIO_U8_INIT_PULLUP                /**********PINA5******/
#define DIO_U8_PIN6_INIT_TYPE       DIO_U8_INIT_PULLUP                /**********PINA6******/
#define DIO_U8_PIN7_INIT_TYPE       DIO_U8_INIT_PULLUP                /**********PINA7******/


/****************  Group B       -> type  ***********************/
#define DIO_U8_PIN8_INIT_TYPE       DIO_U8_INIT_PULLUP          /**********PINB0***********************/
#define DIO_U8_PIN9_INIT_TYPE       DIO_U8_INIT_PULLUP          /**********PINB1***********************/
#define DIO_U8_PIN10_INIT_TYPE      DIO_U8_INIT_PULLUP           /**********PINB2***********************/
#define DIO_U8_PIN11_INIT_TYPE      DIO_U8_INIT_PULLUP           /**********PINB3***********************/
#define DIO_U8_PIN12_INIT_TYPE      DIO_U8_INIT_PULLUP           /**********PINB4***********************/
#define DIO_U8_PIN13_INIT_TYPE      DIO_U8_INIT_PULLUP           /**********PINB5***********************/
#define DIO_U8_PIN14_INIT_TYPE      DIO_U8_INIT_PULLUP           /**********PINB6***********************/
#define DIO_U8_PIN15_INIT_TYPE      DIO_U8_INIT_PULLUP           /**********PINB7***********************/

/****************  Group C       -> type  ************************************/
#define DIO_U8_PIN16_INIT_TYPE     DIO_U8_INIT_PULLUP           /**********PINC0***********************/
#define DIO_U8_PIN17_INIT_TYPE     DIO_U8_INIT_PULLUP           /**********PINC1***********************/
#define DIO_U8_PIN18_INIT_TYPE     DIO_U8_INIT_PULLUP           /**********PINC2***********************/
#define DIO_U8_PIN19_INIT_TYPE     DIO_U8_INIT_PULLUP           /**********PINC3***********************/
#define DIO_U8_PIN20_INIT_TYPE     DIO_U8_INIT_PULLUP           /**********PINC4***********************/
#define DIO_U8_PIN21_INIT_TYPE     DIO_U8_INIT_PULLUP           /**********PINC5***********************/
#define DIO_U8_PIN22_INIT_TYPE     DIO_U8_INIT_PULLUP           /**********PINC6***********************/
#define DIO_U8_PIN23_INIT_TYPE     DIO_U8_INIT_PULLUP           /**********PINC7***********************/

/****************  Group D      -> type  ***********************/
#define DIO_U8_PIN24_INIT_TYPE      DIO_U8_INIT_PULLUP          /**********PIND0***********************/
#define DIO_U8_PIN25_INIT_TYPE      DIO_U8_INIT_PULLUP          /**********PIND1***********************/
#define DIO_U8_PIN26_INIT_TYPE      DIO_U8_INIT_PULLUP          /**********PIND2***********************/
#define DIO_U8_PIN27_INIT_TYPE      DIO_U8_INIT_PULLUP          /**********PIND3***********************/
#define DIO_U8_PIN28_INIT_TYPE      DIO_U8_INIT_PULLUP          /**********PIND4***********************/
#define DIO_U8_PIN29_INIT_TYPE      DIO_U8_INIT_PULLUP          /**********PIND5***********************/
#define DIO_U8_PIN30_INIT_TYPE      DIO_U8_INIT_PULLUP          /**********PIND6***********************/
#define DIO_U8_PIN31_INIT_TYPE      DIO_U8_INIT_PULLUP




  /****************  Port       -> Type  ********************************/
#define DIO_PORT_INIT_TYPE   DIO_U8_INIT_PULLUP



/*****************************************************************************/






/*****************************END OF FILE*******************************/

#endif /* DIO_CONFIGURATION_H_ */
#endif
