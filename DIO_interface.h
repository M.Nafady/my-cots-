/*******************************************************************/
/* Author: M.Nafady                                                */
/* Date  : 29 Jan 2019                                             */
/* Version : v01                                                   */
/********************************************************************/
#ifndef DIO_INTERFACE_H_
#define DIO_INTERFACE_H_
/*******************************************/

/*********************Direction of pins************************/
#define DIO_U8_INPUT    (u8)0   /***setting direction of pin********/
#define DIO_U8_OUTPUT   (u8)1   /***setting direction of pin********/
/******************************************/

 /******************** pins state******************************/
#define DIO_U8_PINHIGH  (u8)1      /***setting pin -> 5v *********/
#define DIO_U8_PINLOW   (u8)0      /***setting pin -> 0v ********/
/*****************************************/

/******************** ports state******************************/
#define DIO_U8_PORTHIGH (u8)0xFF   /***setting all pins of port -> 5v ********/
#define DIO_U8_PORTLOW  (u8)0x00   /***setting all pins of port -> 0v ********/
/****************************************/

/********************Direction of ports**************************/
#define DIO_U8_PORTOUTPUT (u8)0xFF  /*** setting direction of port ******/
#define DIO_U8_PORTINPUT  (u8)0x00  /*** setting direction of port ******/
/****************************************************/

/***********************constrains********************************/

#define DIO_U8_MAXPINNB  (u8)32  /*** maximum number of pins ->32******/
#define DIO_U8_MAXPORTNB (u8)4   /*** maximum number of ports -> 4     */
                             /* 0 -> PORTA  1->PORTB  2->PORTC 3 -> PORTD*/
/***********************************************************************/


/************Group A********************************/
#define DIO_U8_PIN0  (u8)0     /**********PINA0***********************/
#define DIO_U8_PIN1  (u8)1     /**********PINA1***********************/
#define DIO_U8_PIN2  (u8)2     /**********PINA2***********************/
#define DIO_U8_PIN3  (u8)3     /**********PINA3***********************/
#define DIO_U8_PIN4  (u8)4     /**********PINA4***********************/
#define DIO_U8_PIN5  (u8)5     /**********PINA5***********************/
#define DIO_U8_PIN6  (u8)6     /**********PINA6***********************/
#define DIO_U8_PIN7  (u8)7     /**********PINA7***********************/
/****************************************************/

/************Group B********************************/
#define DIO_U8_PIN8  (u8)8        /**********PINB0***********************/
#define DIO_U8_PIN9  (u8)9        /**********PINB1***********************/
#define DIO_U8_PIN10 (u8)10       /**********PINB2***********************/
#define DIO_U8_PIN11 (u8)11       /**********PINB3***********************/
#define DIO_U8_PIN12 (u8)12       /**********PINB4***********************/
#define DIO_U8_PIN13 (u8)13       /**********PINB5***********************/
#define DIO_U8_PIN14 (u8)14       /**********PINB6***********************/
#define DIO_U8_PIN15 (u8)15       /**********PINB7***********************/
/****************************************************/

/************Group C********************************/
#define DIO_U8_PIN16 (u8)16        /**********PINC0***********************/
#define DIO_U8_PIN17 (u8)17        /**********PINC1***********************/
#define DIO_U8_PIN18 (u8)18        /**********PINC2***********************/
#define DIO_U8_PIN19 (u8)19        /**********PINC3***********************/
#define DIO_U8_PIN20 (u8)20        /**********PINC4***********************/
#define DIO_U8_PIN21 (u8)21        /**********PINC5***********************/
#define DIO_U8_PIN22 (u8)22        /**********PINC6***********************/
#define DIO_U8_PIN23 (u8)23        /**********PINC7***********************/
/****************************************************/
/************Group D********************************/
#define DIO_U8_PIN24 (u8)24         /**********PIND0***********************/
#define DIO_U8_PIN25 (u8)25         /**********PIND1***********************/
#define DIO_U8_PIN26 (u8)26         /**********PIND2***********************/
#define DIO_U8_PIN27 (u8)27         /**********PIND3***********************/
#define DIO_U8_PIN28 (u8)28         /**********PIND4***********************/
#define DIO_U8_PIN29 (u8)29         /**********PIND5***********************/
#define DIO_U8_PIN30 (u8)30         /**********PIND6***********************/
#define DIO_U8_PIN31 (u8)31         /**********PIND7***********************/
/**************************************************/




/******************SELECTING PORTS******************************/
#define DIO_U8_PORT_A                  (u8)0        /*portA*/
#define DIO_U8_PORT_B                  (u8)1        /*portB*/
#define DIO_U8_PORT_C                  (u8)2        /*portC*/
#define DIO_U8_PORT_D                  (u8)3        /*portD*/

/**************************************************************/



/*****************************************************************/
/* Description : initialisation of  DIO                           */
/* Input       :   N/A                                           */
/* return      :   N/A                                           */
/* Range       :  Direction  :     DIO_U8_PIN_INIT_INPUT         */
/*                                 DIO_U8_PIN_INIT_OUTPUT        */
/*                                                               */
/*               Value       :     DIO_U8_PIN_INIT_HIGH          */
/*                                 DIO_U8_PIN_INIT_LOW           */
/*****************************************************************/

void DIO_VoidInit(void);
/**************************************************/



/**************************************************/
/* Description : Set pin Direction
 * Input        :   Nb of pin and direction
 * return       :     error state  Ok -> 0
 * 								   notok ->1 */
/**************************************************/

u8 DIO_u8SetPinDirection(u8 Copy_u8PinNb, u8 Copy_u8Direction);



/*****************************************************/
/* Description : Set pin Value                       */
/* Input        :   Nb of pin and value              */
/* return       :     error state       Ok -> 0     */
/* 								   notok ->1        */
/****************************************************/
u8 DIO_u8SetPinValue (u8 Copy_u8PinNb, u8 Copy_u8PinValue);



/*********************************************************/
/* Description : Get pin Value                          */
/* Input        :   Nb of pin and pointer to pin value  */
/* return       :     error state       Ok -> 0        */
/* 								   notok ->1           */
/******************************************************/
u8 DIO_u8GetPinValue(u8 Copy_u8PinNb,u8 *Copy_pu8PinValue);



/*******************************************************/
/* Description : Set port Direction                    */
/* Input        :   Nb of port and port direction      */
/* return       :     error state       Ok -> 0        */
/* 								   notok ->1           */
/*******************************************************/
u8 DIO_u8SetPortDirection(u8 Copy_u8PortNb,u8 Copy_u8Direction);



/*******************************************************/
/* Description : Set port Value                        */
/* Input        :   NB of port and port value          */
/* return       :     error state       Ok -> 0        */
/* 								        notok ->1      */
/********************************************************/

u8 DIO_u8SetPortValue (u8 Copy_u8PortNb, u8 Copy_u8PortValue);



/***********************************************************/
/* Description : Get port Value                            */
/* Input        :   NB of port and pointer to port Value   */
/* return       :     error state       Ok -> 0            */
/* 								   notok ->1               */
/***********************************************************/
u8 DIO_u8GetPortValue(u8 Copy_u8PortNb,u8 *Copy_pu8PortValue);

/*************************End of File************************/

#endif /* DIO_INTERFACE_H_ */
