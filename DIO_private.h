/*******************************************************************/
/* Author: M.Nafady                                                */
/* Date  : 29 Jan 2019                                              */
/* File  :private.h                                                */
/* Version : v01                                                   */
/********************************************************************/

#ifndef DIO_PRIVATE_H_
#define DIO_PRIVATE_H_
#include"DIO_configuration.h"

/**************** Setting Direction   ********************************/
#define DIO_U8_DDRA      ((Register*)0x3A)     /*portA*/
#define DIO_U8_DDRB      ((Register*)0x37 )     /*portB*/
#define DIO_U8_DDRC      ((Register*)0x34)      /*portC*/
#define DIO_U8_DDRD      ((Register*)0x31)      /*portD*/

/**************** Setting Value    ********************************/
#define DIO_U8_PORTA      ((Register*)0x3B)    /*portA*/
#define DIO_U8_PORTB      ((Register*)0x38 )   /*portB*/
#define DIO_U8_PORTC      ((Register*)0x35)    /*portC*/
#define DIO_U8_PORTD     ((Register*)0x32 )    /*portD*/

/****************  Setting input   ********************************/

#define DIO_U8_PINA      ((Register*)0x39)     /*portA*/
#define DIO_U8_PINB      ((Register*)0x36 )   /*portB*/
#define DIO_U8_PINC      ((Register*)0x33)    /*portC*/
#define DIO_U8_PIND     ((Register*)0x30)     /*portD*/


/************SETTING  DEFAULT VALUES AND DIRECTION****************/
#define DIO_U8_PIN_INIT_HIGH           1       /*initial value for pin -> high       */
#define DIO_U8_PIN_INIT_LOW            0       /*initial value for pin -> low        */
#define DIO_U8_PIN_INIT_INPUT          0       /*initial direction for pin -> input  */
#define DIO_U8_PIN_INIT_OUTPUT         1       /*initial direction for pin -> output */
#define DIO_U8_PIN_DEF_VALUE           1       /*default value for pin               */
/******************************************************************/
/*************************Selecting type *********************/
#define DIO_U8_INIT_PULLUP     (u8)0
#define DIO_U8_INIT_PULLDOWN   (u8)1
/**********************************************************/


#define DIO_NB_OF_PINS_IN_PORT      (u8)8       /*MAXIMUM PINS IN PORT */





/************************END OF FILE**********************************/

#endif /* DIO_PRIVATE_H_ */
