/*******************************************************************/
/* Author: M.Nafady                                                */
/* Date  : 29 Jan 2019                                             */
/* Version : v01                                                   */
/********************************************************************/

#include "STD_TYPES.h"
#include "BIT_CALC.h"
#include "REGISTER_CTRL.h"
#include "DIO_interface.h"
#include "DIO_private.h"




/*****************************************************************/
/* Description : initialisation of  DIO                           */
/* Input       :   N/A                                           */
/* return      :   N/A                                           */
/* Range       :  Direction  :     DIO_U8_PIN_INIT_INPUT         */
/*                                 DIO_U8_PIN_INIT_OUTPUT        */
/*                                                               */
/*               Value       :     DIO_U8_PIN_INIT_HIGH          */
/*                                 DIO_U8_PIN_INIT_LOW           */
/*****************************************************************/

void DIO_VoidInit(void){


 /* Setting  PortA direction*/
DIO_U8_DDRA -> ByteAccess= BIT_CALC_CONC_8BIT(DIO_U8_PIN0_INIT_DIRECTION,
		                                      DIO_U8_PIN1_INIT_DIRECTION,
                                              DIO_U8_PIN2_INIT_DIRECTION,
                                              DIO_U8_PIN3_INIT_DIRECTION,
										      DIO_U8_PIN4_INIT_DIRECTION,
										      DIO_U8_PIN5_INIT_DIRECTION,
										      DIO_U8_PIN6_INIT_DIRECTION,
										      DIO_U8_PIN7_INIT_DIRECTION);
	/* Setting  PortB direction*/
	DIO_U8_DDRB -> ByteAccess= BIT_CALC_CONC_8BIT(DIO_U8_PIN8_INIT_DIRECTION,
                                                  DIO_U8_PIN9_INIT_DIRECTION,
                                                  DIO_U8_PIN10_INIT_DIRECTION,
                                                  DIO_U8_PIN11_INIT_DIRECTION,
			                                      DIO_U8_PIN12_INIT_DIRECTION,
			                                      DIO_U8_PIN13_INIT_DIRECTION,
			                                      DIO_U8_PIN14_INIT_DIRECTION,
			                                      DIO_U8_PIN15_INIT_DIRECTION);

	/* Setting  PortC direction*/
    DIO_U8_DDRC  -> ByteAccess= BIT_CALC_CONC_8BIT(DIO_U8_PIN16_INIT_DIRECTION,
	                                               DIO_U8_PIN17_INIT_DIRECTION,
                                                   DIO_U8_PIN18_INIT_DIRECTION,
                                                   DIO_U8_PIN19_INIT_DIRECTION,
                                                   DIO_U8_PIN20_INIT_DIRECTION,
                                                   DIO_U8_PIN21_INIT_DIRECTION,
                                                   DIO_U8_PIN22_INIT_DIRECTION,
                                                   DIO_U8_PIN23_INIT_DIRECTION);



    /* Setting  PortD direction*/
   DIO_U8_DDRD  -> ByteAccess= BIT_CALC_CONC_8BIT(DIO_U8_PIN24_INIT_DIRECTION,
    	                                          DIO_U8_PIN25_INIT_DIRECTION,
                                                  DIO_U8_PIN26_INIT_DIRECTION,
                                                  DIO_U8_PIN27_INIT_DIRECTION,
                                                  DIO_U8_PIN28_INIT_DIRECTION,
                                                  DIO_U8_PIN29_INIT_DIRECTION,
                                                  DIO_U8_PIN30_INIT_DIRECTION,
                                                  DIO_U8_PIN31_INIT_DIRECTION);
/**************************************************************************/
/*checking if direction of pin -> input and not setting value at the same time */
/*************************************************************************/

#if (DIO_U8_PIN0_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN0_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN0 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN0_INIT_VALUE
#define DIO_U8_PIN0_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN1_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN1_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN1 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN1_INIT_VALUE
#define DIO_U8_PIN1_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN2_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN2_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN2 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN2_INIT_VALUE
#define DIO_U8_PIN2_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN3_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN3_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN3 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN3_INIT_VALUE
#define DIO_U8_PIN3_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN4_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN4_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN4 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN4_INIT_VALUE
#define DIO_U8_PIN4_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN5_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN5_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN5 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN5_INIT_VALUE
#define DIO_U8_PIN5_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN6_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN6_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN6 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN6_INIT_VALUE
#define DIO_U8_PIN6_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif
#if (DIO_U8_PIN7_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN7_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN7 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN7_INIT_VALUE
#define DIO_U8_PIN7_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN8_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN8_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN8 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN8_INIT_VALUE
#define DIO_U8_PIN8_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN9_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN9_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN9 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN9_INIT_VALUE
#define DIO_U8_PIN9_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN10_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN10_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN10 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN10_INIT_VALUE
#define DIO_U8_PIN10_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN11_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN11_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN11 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN11_INIT_VALUE
#define DIO_U8_PIN11_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN12_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN12_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN12 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN12_INIT_VALUE
#define DIO_U8_PIN12_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN13_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN13_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN13 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN13_INIT_VALUE
#define DIO_U8_PIN13_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN14_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN14_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN14 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN14_INIT_VALUE
#define DIO_U8_PIN14_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN15_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN15_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN15 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN15_INIT_VALUE
#define DIO_U8_PIN15_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN16_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN16_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN16 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN16_INIT_VALUE
#define DIO_U8_PIN16_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN17_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN17_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN17 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN17_INIT_VALUE
#define DIO_U8_PIN17_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN18_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN18_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN18 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN18_INIT_VALUE
#define DIO_U8_PIN18_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN19_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN19_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN19 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN19_INIT_VALUE
#define DIO_U8_PIN19_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN20_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN20_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN20 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN20_INIT_VALUE
#define DIO_U8_PIN20_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN21_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN21_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN21 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN21_INIT_VALUE
#define DIO_U8_PIN21_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN22_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN22_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN22 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN22_INIT_VALUE
#define DIO_U8_PIN22_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN23_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN23_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN23 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN23_INIT_VALUE
#define DIO_U8_PIN23_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN24_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN24_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN24 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN24_INIT_VALUE
#define DIO_U8_PIN24_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN25_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN25_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN25 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN25_INIT_VALUE
#define DIO_U8_PIN25_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN26_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN26_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN26 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN26_INIT_VALUE
#define DIO_U8_PIN26_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN27_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN27_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN27 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN27_INIT_VALUE
#define DIO_U8_PIN27_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN28_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN28_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN28 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN28_INIT_VALUE
#define DIO_U8_PIN28_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN29_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN29_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN29 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN29_INIT_VALUE
#define DIO_U8_PIN29_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN30_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN30_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN30 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN30_INIT_VALUE
#define DIO_U8_PIN30_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

#if (DIO_U8_PIN31_INIT_DIRECTION==DIO_U8_PIN_INIT_INPUT)&&(DIO_U8_PIN31_INIT_VALUE!=DIO_U8_PIN_DEF_VALUE)
#warning "YOU ARENOT INSERTING AT PIN31 VALUE WHILE IT IS INPUT"
#undef DIO_U8_PIN31_INIT_VALUE
#define DIO_U8_PIN31_INIT_VALUE     DIO_U8_PIN_DEF_VALUE
#endif

/*****************************************************************************************/



/* Setting  PortA Value*/
DIO_U8_PORTA -> ByteAccess=BIT_CALC_CONC_8BIT( DIO_U8_PIN0_INIT_VALUE,
		                                       DIO_U8_PIN1_INIT_VALUE,
		                                       DIO_U8_PIN2_INIT_VALUE,
		                                       DIO_U8_PIN3_INIT_VALUE,
		                                       DIO_U8_PIN4_INIT_VALUE,
		                                       DIO_U8_PIN5_INIT_VALUE,
		                                       DIO_U8_PIN6_INIT_VALUE,
		                                       DIO_U8_PIN7_INIT_VALUE);

/* Setting  PortB Value*/
DIO_U8_PORTB -> ByteAccess=BIT_CALC_CONC_8BIT( DIO_U8_PIN8_INIT_VALUE,
	                                           DIO_U8_PIN9_INIT_VALUE,
	                                           DIO_U8_PIN10_INIT_VALUE,
	                                           DIO_U8_PIN11_INIT_VALUE,
                                               DIO_U8_PIN12_INIT_VALUE,
                                               DIO_U8_PIN13_INIT_VALUE,
                                               DIO_U8_PIN14_INIT_VALUE,
                                               DIO_U8_PIN15_INIT_VALUE);

/* Setting  PortC Value*/
DIO_U8_PORTC -> ByteAccess=BIT_CALC_CONC_8BIT(DIO_U8_PIN16_INIT_VALUE,
	                                          DIO_U8_PIN17_INIT_VALUE,
	                                          DIO_U8_PIN18_INIT_VALUE,
	                                          DIO_U8_PIN19_INIT_VALUE,
                                              DIO_U8_PIN20_INIT_VALUE,
                                              DIO_U8_PIN21_INIT_VALUE,
                                              DIO_U8_PIN22_INIT_VALUE,
                                              DIO_U8_PIN23_INIT_VALUE);







/* Setting  PortD Value*/
DIO_U8_PORTD -> ByteAccess=BIT_CALC_CONC_8BIT(DIO_U8_PIN24_INIT_VALUE,
	                                          DIO_U8_PIN25_INIT_VALUE,
	                                          DIO_U8_PIN26_INIT_VALUE,
	                                          DIO_U8_PIN27_INIT_VALUE,
                                              DIO_U8_PIN28_INIT_VALUE,
                                              DIO_U8_PIN29_INIT_VALUE,
                                              DIO_U8_PIN30_INIT_VALUE,
                                              DIO_U8_PIN31_INIT_VALUE);








return;
	}





/***********************************************************************/
 /* Description : Set pin Direction                                    */
 /* Inputs : u8PinNB : pin Number  &   Copy_u8Direction: pin direction*/
 /* Output :Error State												  */
 /********************************************************************/


u8 DIO_u8SetPinDirection(u8 Copy_u8PinNb, u8 Copy_u8Direction){

/*************************Local Variables***************************/

u8 Local_u8Error;
u8 Local_u8PortId;
u8 Local_u8PinId;

/********************************************************************/

if ((Copy_u8PinNb>=DIO_U8_MAXPINNB) ||
((Copy_u8Direction < DIO_U8_PORTINPUT) && (Copy_u8Direction > DIO_U8_PORTOUTPUT)))
{
Local_u8Error=STD_TYPES_U8_ERROR_NOK;
}
else
{
	 Local_u8PinId= (Copy_u8PinNb)%(DIO_NB_OF_PINS_IN_PORT);
	 Local_u8PortId = (Copy_u8PinNb)/(DIO_NB_OF_PINS_IN_PORT);
     switch (Local_u8PortId)
     {

     case DIO_U8_PORT_A  :
    	 BIT_CALC_ASSIGN_BIT(DIO_U8_DDRA->ByteAccess,Local_u8PinId,Copy_u8Direction);
            Local_u8Error=STD_TYPES_U8_ERROR_OK;
            break;
     case DIO_U8_PORT_B  :
    	 BIT_CALC_ASSIGN_BIT(DIO_U8_DDRB->ByteAccess,Local_u8PinId,Copy_u8Direction);
    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;
    	             break;
     case DIO_U8_PORT_C  :
    	 BIT_CALC_ASSIGN_BIT(DIO_U8_DDRC->ByteAccess,Local_u8PinId,Copy_u8Direction);
    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;
    	             break;
     case DIO_U8_PORT_D :
    	 BIT_CALC_ASSIGN_BIT(DIO_U8_DDRD->ByteAccess,Local_u8PinId,Copy_u8Direction);
    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;


     }
}




return Local_u8Error;
}



/***********************************************************************/
/* Description : Set Value                                            */
/* Inputs : u8PinNB : pin Number  &   Copy_u8PinValue: pin value      */
/* Output :Error State												  */
/**********************************************************************/

u8 DIO_u8SetPinValue (u8 Copy_u8PinNb, u8 Copy_u8PinValue){

/**********Local Variables**********/

	u8 Local_u8Error;
	u8 Local_u8PortId;
	u8 Local_u8PinId;

/***********************************/
if ((Copy_u8PinNb>=DIO_U8_MAXPINNB) ||
		((Copy_u8PinValue!=DIO_U8_PINHIGH) && (Copy_u8PinValue!=DIO_U8_PINLOW)))
		{
		Local_u8Error=STD_TYPES_U8_ERROR_NOK;

		}
	else
	{
		 Local_u8PinId= (Copy_u8PinNb)%DIO_NB_OF_PINS_IN_PORT;
		 Local_u8PortId = (Copy_u8PinNb)/DIO_NB_OF_PINS_IN_PORT;
	     switch (Local_u8PortId)
	     {

	     case DIO_U8_PORT_A :
	    	 BIT_CALC_ASSIGN_BIT(DIO_U8_PORTA->ByteAccess,Local_u8PinId,Copy_u8PinValue);
	            Local_u8Error=STD_TYPES_U8_ERROR_OK;
	            break;
	     case DIO_U8_PORT_B :
	    	 BIT_CALC_ASSIGN_BIT(DIO_U8_PORTB->ByteAccess,Local_u8PinId,Copy_u8PinValue);
	    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;
	    	             break;
	     case DIO_U8_PORT_C :
	    	 BIT_CALC_ASSIGN_BIT(DIO_U8_PORTC->ByteAccess,Local_u8PinId,Copy_u8PinValue);
	    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;
	    	             break;
	     case DIO_U8_PORT_D :
	    	 BIT_CALC_ASSIGN_BIT(DIO_U8_PORTD->ByteAccess,Local_u8PinId,Copy_u8PinValue);
	    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;


	     }
	}

	return Local_u8Error;
	}



/***********************************************************************/
/* Description : Get Value                                            */
/* Inputs : u8PinNB : pin Number  &   *Copy_pu8PinValue: pin value      */
/* Output :Error State												  */
/**********************************************************************/

u8 DIO_u8GetPinValue(u8 Copy_u8PinNb,u8 *Copy_pu8PinValue){

/******************Local Variables******************************/

	u8 Local_u8Error;
	u8 Local_u8PortId;
	u8 Local_u8PinId;

/******************************************************************/
if ((Copy_u8PinNb>=DIO_U8_MAXPINNB)||(Copy_pu8PinValue==(STD_TYPES_NULL)))
{
		Local_u8Error=STD_TYPES_U8_ERROR_NOK;
		}
	else
	{
		 Local_u8PinId= (Copy_u8PinNb)%(DIO_NB_OF_PINS_IN_PORT);
		 Local_u8PortId = (Copy_u8PinNb)/(DIO_NB_OF_PINS_IN_PORT);
	     switch (Local_u8PortId)
	     {

	     case DIO_U8_PORT_A :
	            *Copy_pu8PinValue=BIT_CALC_GET_BIT(DIO_U8_PINA->ByteAccess,Local_u8PinId);
	            Local_u8Error=STD_TYPES_U8_ERROR_OK;
	            break;
	     case DIO_U8_PORT_B :
	    	 *Copy_pu8PinValue=BIT_CALC_GET_BIT(DIO_U8_PINB->ByteAccess,Local_u8PinId);
	    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;
	    	             break;
	     case DIO_U8_PORT_C :
	    	 *Copy_pu8PinValue=BIT_CALC_GET_BIT(DIO_U8_PINC->ByteAccess,Local_u8PinId);
	    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;
	    	             break;
	     case DIO_U8_PORT_D :
	    	 *Copy_pu8PinValue=BIT_CALC_GET_BIT(DIO_U8_PIND->ByteAccess,Local_u8PinId);
	    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;


	     }
	}

	return Local_u8Error;
}




/***********************************************************************/
/* Description : Set Direction                                            */
/* Inputs : Copy_u8PortNb,: portNB  &   Copy_u8Direction: port direction      */
/* Output :Error State												  */
/**********************************************************************/


u8 DIO_u8SetPortDirection(u8 Copy_u8PortNb,u8 Copy_u8Direction){



/*************************Local Variables***************************/

u8 Local_u8Error;


/********************************************************************/

if ((Copy_u8PortNb>=DIO_U8_MAXPORTNB) ||
		((Copy_u8Direction < DIO_U8_PORTINPUT) && (Copy_u8Direction > DIO_U8_PORTOUTPUT))){
Local_u8Error=STD_TYPES_U8_ERROR_NOK;
}
else
{
     switch (Copy_u8PortNb)
     {

     case DIO_U8_PORT_A :

            DIO_U8_DDRA->ByteAccess =Copy_u8Direction;
            Local_u8Error=STD_TYPES_U8_ERROR_OK;
            break;
     case DIO_U8_PORT_B :
    	 DIO_U8_DDRB->ByteAccess =Copy_u8Direction;
    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;
    	             break;
     case DIO_U8_PORT_C :
    	 DIO_U8_DDRC->ByteAccess =Copy_u8Direction;
    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;
    	             break;
     case DIO_U8_PORT_D :
    	 DIO_U8_DDRD ->ByteAccess=Copy_u8Direction;
    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;


     }
}




return Local_u8Error;
}




/***********************************************************************/
/* Description : Set Value                                            */
/* Inputs : Copy_u8PortNb,: portNB  &   Copy_u8PortValue: portvalue    */
/* Output :Error State												  */
/**********************************************************************/

u8 DIO_u8SetPortValue (u8 Copy_u8PortNb, u8 Copy_u8PortValue){

	/*Local Variables*/

	u8 Local_u8Error;
/****************************************/



if ((Copy_u8PortNb>=DIO_U8_MAXPORTNB) ||
	((Copy_u8PortValue > DIO_U8_PORTHIGH) &&
	(Copy_u8PortValue < DIO_U8_PORTLOW)))
		{
			Local_u8Error=STD_TYPES_U8_ERROR_NOK;

		}
	else
	{

	     switch (Copy_u8PortNb)
	     {

	     case DIO_U8_PORT_A :
	            DIO_U8_PORTA ->ByteAccess= Copy_u8PortValue;
	            Local_u8Error=STD_TYPES_U8_ERROR_OK;
	            break;
	     case DIO_U8_PORT_B :
	    	 DIO_U8_PORTB ->ByteAccess= Copy_u8PortValue;
	    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;
	    	             break;
	     case DIO_U8_PORT_C :
	    	 DIO_U8_PORTC ->ByteAccess= Copy_u8PortValue;
	    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;
	    	             break;
	     case DIO_U8_PORT_D :
	    	 DIO_U8_PORTD->ByteAccess = Copy_u8PortValue;
	    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;


	     }
	}

	return Local_u8Error;
	}
/***********************************************************************/
/* Description : Get Value                                            */
/* Inputs : Copy_u8PortNb,: portNB  &   *Copy_pu8PortValue:portvalue    */
/* Output :Error State												  */
/**********************************************************************/

u8 DIO_u8GetPortValue(u8 Copy_u8PortNb,u8 *Copy_pu8PortValue){

	/***********************Local Variables**************/

	u8 Local_u8Error;


/*********************************************************/


if ((Copy_u8PortNb>=DIO_U8_MAXPORTNB)||(Copy_pu8PortValue==(STD_TYPES_NULL)))
{
		Local_u8Error=STD_TYPES_U8_ERROR_NOK;
		}
	else
	{
	     switch (Copy_u8PortNb)
	     {

	     case DIO_U8_PORT_A :
	            *Copy_pu8PortValue=DIO_U8_PINA->ByteAccess;
	            Local_u8Error=STD_TYPES_U8_ERROR_OK;
	            break;
	     case DIO_U8_PORT_B :
	    	 *Copy_pu8PortValue=DIO_U8_PINB->ByteAccess;
	    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;
	    	             break;
	     case DIO_U8_PORT_C :
	    	 *Copy_pu8PortValue=DIO_U8_PIND->ByteAccess;
	    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;
	    	             break;
	     case DIO_U8_PORT_D :
	    	 *Copy_pu8PortValue=DIO_U8_PINC->ByteAccess;
	    	 Local_u8Error=STD_TYPES_U8_ERROR_OK;


	     }
	}

	return Local_u8Error;
}


/*********************************END OF FILE*****************************/

